/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.xo_oop;

/**
 *
 * @author THANAWAT_TH
 */
public class User {
    
    
    private char name;
    private int win;
    private int lose;
    private int draw;
    
    User(char name){
        this.name=name;
    }
  
   public char getName() {
        return name;
    }
   public int win() {
        return win++;
    }

   public int lose() {
        return lose++;
    }

   public int draw() {
        return draw++;
    }

   public void setName(char name) {
        this.name = name;
    }

   
}
