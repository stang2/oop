/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.xo_oop;

/**
 *
 * @author THANAWAT_TH
 */
import java.util.Scanner;

public class Game {
    Scanner s = new Scanner(System.in);
    User X;
    User O;
    User turn;
    Table table;
    Game(){  
    this.X = new User('X');
    this.O = new User('O');
    table = new Table(X,O);

    }
    void showWelcome(){
        System.out.println("--- Welcome to OX Game ---");
    }

    void showTable(){
        table.showTable();
    }
    void showTurn() {
        System.out.println("This turn is :" + table.getCurrentPlayer().getName());
    }

    void input() {

        while (true) {
            System.out.println("Please input Row and Col :");
            
            int row = s.nextInt() - 1;
            int col = s.nextInt() - 1;

            if (table.setRowCol(row, col)) {
                break;
            } else {
                System.out.println("You input missmath or Error Input.");
            }
        }
        while(true){
            this.showWelcome();
            this.showTable();
            this.showTurn();
            this.input();
            table.checkWin();
            if(table.isFinish){
                if(table.getWinner()==null){
                    System.out.println("Draw");
                    System.out.println("Do you want to play new Game ??");
                    System.out.println("Y or N");
                    char newGame = s.next().charAt(0);
                    if(newGame=='Y'){
                        newGame();
                    }else{
                        System.out.println("Thank you for play this game.");
                    }
                }else{
                    this.showTable();
                    System.out.println(table.getWinner().getName()+" Win!!");
                    
                    System.out.println("Do you want to play new Game ??");
                    System.out.println("Y or N");
                    char newGame = s.next().charAt(0);
                    if(newGame=='Y'){
                        newGame();
                    }else{
                        System.out.println("Thank you for play this game.");
                    }
                }
                break;
            }

            table.switchPlayer();
        }
    }

 }

    


